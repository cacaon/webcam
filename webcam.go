package main

import (
	"fmt"
	"os"
	"unsafe"
	//"path"
	//"runtime"

	"github.com/lazywei/go-opencv/opencv"
	//"../opencv" // can be used in forks, comment in real application
)

func main() {
	win := opencv.NewWindow("Go-OpenCV Webcam")
	config := opencv.NewWindow("Config thresholds")
	defer win.Destroy()
	newwin := opencv.NewWindow("test")
	defer newwin.Destroy()

	var threshold [6]int

	config.CreateTrackbar("Red max", 125, 255, func(pos int) {
		threshold[5] = pos
	})
	config.CreateTrackbar("Red min", 125, 255, func(pos int) {
		threshold[4] = pos
	})
	config.CreateTrackbar("Green max", 125, 255, func(pos int) {
		threshold[3] = pos
	})
	config.CreateTrackbar("Green min", 125, 255, func(pos int) {
		threshold[2] = pos
	})

	config.CreateTrackbar("Blue max", 125, 255, func(pos int) {
		threshold[1] = pos
	})
	config.CreateTrackbar("Blue min", 125, 255, func(pos int) {
		threshold[0] = pos
	})

	//	cap := opencv.NewFileCapture("http://localhost:8080/?action=stream?dummy=.mjpg")
	cap := opencv.NewCameraCapture(0)
	if cap == nil {
		panic("can not open camera")
	}
	defer cap.Release()

	//win.CreateTrackbar("Thresh", 1, 100, func(pos int, param ...interface{}) {
	//	edge_threshold = pos
	//})

	fmt.Println("Press ESC to quit")
	for {
		if cap.GrabFrame() {
			img := cap.RetrieveFrame(1)
			if img != nil {
				ProcessImage(img, win, threshold, newwin)
			} else {
				fmt.Println("Image ins nil")
			}
		}
		key := opencv.WaitKey(100)
		if key >= 0 {
			os.Exit(0)
		}
	}
}

// ProcessImage test
func ProcessImage(img *opencv.IplImage, win *opencv.Window, threshold [6]int, newwin *opencv.Window) error {
	//data := img.ImageData()
	var s [4]float64
	var boolList [640][480]bool
	color := opencv.NewScalar(255, 255, 255, 255)
	newImage := opencv.CreateImage(640, 480, 8, 4)
	defer newImage.Release()
	//	var r, g, b float64
	for i := 0; i < img.Width(); i++ {
		for j := 0; j < img.Height(); j++ {
			s = img.Get2D(i, j).Val()
			if s[0] <= float64(threshold[1]) && s[1] <= float64(threshold[3]) && s[2] <= float64(threshold[5]) &&
				s[0] >= float64(threshold[0]) && s[1] >= float64(threshold[2]) && s[2] >= float64(threshold[4]) {
				boolList[i][j] = true
			}
		}
	}
	for i := 0; i < img.Width(); i++ {
		for j := 0; j < img.Height(); j++ {
			if boolList[i][j] {
				newImage.Set2D(i, j, color)
			}
		}
	}
	s = img.Get2D(0, 0).Val()
	fmt.Printf("pointer access %d %d %d\n", *(*uint8)((unsafe.Pointer)((uintptr)(img.ImageData()))), *(*uint8)((unsafe.Pointer)((uintptr)(img.ImageData()) + 1)), *(*uint8)((unsafe.Pointer)((uintptr)(img.ImageData()) + 2)))
	fmt.Printf("API access %f %f %f\n\n", s[0], s[1], s[2])
	newwin.ShowImage(newImage)
	win.ShowImage(img)
	return nil
}

// Dilate is dilate
// func Dilate(img *opencv.IplImage) {
// 	for i := 1; i < img.Width()-1; i++ {
// 		for j := 1; j < img.Height()-1; j++ {
// 			if(img)
// 		}
// 	}
// }
// void dilate(img *src){
//         bool **before;
//         before = new bool*[src->height];
//
//         for(int i = 0; i < src->height; i++){
//                 before[i] = new bool[src->width];
//                 for(int j = 0; j < src->width; j++){
//                         before[i][j] = src->data[i][j].r == 255 && src->data[i][j].g == 0 && src->data[i][j].b == 0;
//                 }
//         }
//
//         for(int i = 1; i < src->height - 1; i++)
//         {
//                 for(int j = 1; j < src->width - 1; j++)
//                 {
//                         if(before[i-1][j-1] || before[i-1][j] || before[i-1][j+1] ||
//                                 before[i][j-1] || before[i][j] || before[i][j+1] ||
//                                 before[i+1][j-1] || before[i+1][j] || before[i+1][j+1]){
//                                         src->data[i][j].r = 255;
//                                         src->data[i][j].g = 0;
//                                         src->data[i][j].b = 0;
//                         }
//                 }
//         }
//
//         delete[] before;
// }
// void erode(img *src){
//         bool **before;
//         before = new bool*[src->height];
//
//         for(int i = 0; i < src->height; i++){
//                 before[i] = new bool[src->width];
//                 for(int j = 0; j < src->width; j++){
//                         before[i][j] = src->data[i][j].r == 255 && src->data[i][j].g == 0 && src->data[i][j].b == 0;
//                 }
//         }
//
//         for(int i = 1; i < src->height - 1; i++)
//         {
//                 for(int j = 1; j < src->width - 1; j++)
//                 {
//                         if(before[i-1][j-1] && before[i-1][j] && before[i-1][j+1] &&
//                                 before[i][j-1] && before[i][j] && before[i][j+1] &&
//                                 before[i+1][j-1] && before[i+1][j] && before[i+1][j+1]){
//                                         src->data[i][j].r = 255;
//                                         src->data[i][j].g = 0;
//                                         src->data[i][j].b = 0;
//                         }
//                         else{
//                                 src->data[i][j].r = 0;
//                                 src->data[i][j].g = 0;
//                                 src->data[i][j].b = 0;
//                         }
//                 }
//         }
//
//         delete[] before;
// }
//
// void closing(img* image, const int& n){
//         for(int i = 0; i < n; i++) dilate(image);
//         for(int i = 0; i < n; i++) erode(image);
// }
